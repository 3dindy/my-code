#!/bin/bash
HOMEDIR="/home/student"
GITHUBDIR="/home/student/my-code"
TMPDIR="/tmp/"

cd $GITHUBDIR
cp -r $HOMEDIR/ans/* $GITHUBDIR/ans
cp $HOMEDIR/.bash_history $GITHUBDIR/
git status
git add $GITHUBDIR/*
git commit -m $1
git push origin
cd $HOMEDIR 
